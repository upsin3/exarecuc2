    var edad;
    var altura;
    var peso;
    var imc;
    var x=0;
    var nivel;
    function generador(){

        edad=Math.random()*(99-18)+18;
        edad=edad.toFixed();
        document.getElementById('edad').value=edad;
        console.log(edad);
        altura=Math.random()*(1.5-2.5)+2.5;
        altura=altura.toFixed(1);
        console.log(altura);
        document.getElementById('altura').value=altura;
        peso=Math.random()*(130-20)+20;
        peso=peso.toFixed();
        document.getElementById('peso').value=peso;
        console.log(peso);

    }


    function IMC(){
        imc=peso/(altura*altura);
        imc=imc.toFixed(1);
        document.getElementById('imc').value=imc;
        
        if(imc<18.5)
        {
            document.getElementById('nivel').value="Bajo de peso";
            nivel="Bajo de Peso";
        }
        else if(imc>=18.5 && imc<=24.9)
        {
            document.getElementById('nivel').value="Peso Saludable";
            nivel="Peso Saludable";
        }
        else if(imc>=25.0 && imc<=29.9)
        {
            document.getElementById('nivel').value="Sobre Peso";
            nivel="Sobre Peso";
        }
        else if(imc>=30.0)
        {
            document.getElementById('nivel').value="Obesidad";
            nivel="Obesidad";
        }
        
    }
    function registrar(){

        var tabla=document.getElementById('registro');
    x++;
        tabla.innerHTML=tabla.innerHTML+"<br>"+"Numero "+x+" Edad: "+edad+" Altura: "+altura+" Peso: "+peso+" IMC: "+imc+" Nivel: "+nivel;
    }

    function borrar()
    {
        var tabla=document.getElementById('registro');
        tabla.innerHTML="";
    }

    var btnCalcular=document.getElementById('calcular');
    btnCalcular.addEventListener('click',IMC);

    var btnGenerar=document.getElementById('generar');
    btnGenerar.addEventListener('click',generador);

    var btnRegistrar=document.getElementById('registrar');
    btnRegistrar.addEventListener('click',registrar);

    var btnBorrar=document.getElementById('limpiar');
    btnBorrar.addEventListener('click',borrar)

